﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTA;

namespace BUS
{
    public class RoomMapBUS
    {
        RoomMapDB listroom = new RoomMapDB();

        public List<spGetListRoomOccupied_Result> GetAllListRoomOccupied()
        {
            return listroom.GetListRoomOccupied();
        }
        public spGetInfoOfRoomOccupied_Result GetInfoOfRoom(int? roomid, int? bookingid)
        {
            return listroom.GetInfo(roomid,bookingid);
        }
        public List<spGetListServiceAvaiableBooking_Result> GetlistServiceRoom(int? roomid)
        {
            return listroom.GetListService(roomid);
        }
        public void InsertNewBookingService(int bookingid, int roomid, int serviceid)
        {
            listroom.InsertBookingService(bookingid, roomid, serviceid);
        }

        public void UpdateInvoice(int? bookingid, int? totalservice, decimal? totalpriceservice, decimal? customerpaid, decimal? change_money)
        {
            listroom.UpdateInvoice(bookingid,totalservice,totalpriceservice,customerpaid,change_money);
        }
        public List<spGetRoomOccupied_Result> GetRoomForCheckOut()
        {
            return listroom.GetRoom();
        }

        public List<spGetlistroomCustomerOccupied_Result> GetlistRoomCustomerOccupied(int? booking)
        {
            return listroom.GetReallyRoom(booking);
        }
        public List<spGetlistServiceRoom_Result> GetServiceUse(int? bookingid)
        {
            return listroom.GetService(bookingid);
        }
        public void UpdateCustomerBookingCheckOut(int? bookingid)
        {
            listroom.UpdateBookingCheckOut(bookingid);
        }
    }
}
