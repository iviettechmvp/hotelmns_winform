﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTA;

namespace BUS
{
    public class UserBUS
    {
        UserDB user = new UserDB();
        public bool LoginAuth(string username, string password)
        {
            return user.Login(username, password);
        }
        public string GetForgotPassword(string email)
        {
            return user.GetPassword(email);
        }
        public string GetNameUser(string username)
        {
            return user.GetName(username);
        }
        public void ChangeStatus(string username)
        {
            user.UpdateStatus(username);
        }

        public void LogOutUser(string username)
        {
            user.Logout(username);
        }
        public int UserCount()
        {
            return user.CountUser();
        }
        public int CustomerCount()
        {
            return user.CountCustomer();
        }
        public int BookingCount()
        {
            return user.CountBooking();
        }
        public int RoomCount()
        {
            return user.CountRoom();
        }
        public int ServiceCount()
        {
            return user.CountService();
        }
        public int FacilitiesCount()
        {
            return user.CountFacilities();
        }
    }
}
