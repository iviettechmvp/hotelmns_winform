﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTA;
namespace BUS
{
    public class CustomerInfoBUS
    {
        CustomerInfoDB customer = new CustomerInfoDB();

        public spGetCustomerJustBooking_Result GetInfoCustomerBooking(int? bookingId)
        {
            return customer.GetInfo(bookingId);
        }
        public void ConFirmBooking(int? bookingid, string customerName, int totalroom, decimal totalRoomPrice, decimal totalPrice, decimal totalCustomerpaid, decimal changeMoney, int toalService = 0, decimal totalPriceService = 0)
        {
            customer.ConfirmBookingInvoice(bookingid,customerName, totalroom, totalRoomPrice, totalPrice, totalCustomerpaid, changeMoney, toalService, totalPriceService);
        }
        public List<spGetAllCustomerBookingConfirm_Result> GetAllCustomerBookingConfirmed()
        {
            return customer.GetCustomerConfirm();
        }
        public List<spGetRoomBooked_Result> GetRoomBooking(int? bookingid)
        {
            return customer.GetCustomerRoom(bookingid);
        }
        public void UpdateCustomerBookingCheckIn(int customerId, int? bookingid, string idcard, string idcardType, List<int> roomid)
        {
            customer.UpdateBookingCheckIn(customerId, bookingid, idcard, idcardType, roomid);
        }
    }
}
