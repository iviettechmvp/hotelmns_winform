﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTA;

namespace HotelMNS_WF
{
    public partial class frmShowNotification : MetroFramework.Forms.MetroForm
    {
        private CustomerInfoBUS customer = new CustomerInfoBUS();
        int bookingId;
        decimal totalprice;
        string customerName;
        int totalRoom;
        public frmShowNotification(int idbooking)
        {
            InitializeComponent();
            this.bookingId = idbooking;
        }

        private void frmShowNotification_Load(object sender, EventArgs e)
        {
            LoadInfo();
        }

        public void LoadInfo()
        {
            spGetCustomerJustBooking_Result customerInfo = customerInfo = customer.GetInfoCustomerBooking(bookingId);
            customerName = customerInfo.first_name + " " + customerInfo.last_name;
            lbCustomerName.Text = customerName;
            lbCustomerName.TextAlign = ContentAlignment.MiddleCenter;

            txtEmail.Text = customerInfo.email;
            txtPhoneNumber.Text = customerInfo.phone_number;
            datepickDateCheckIn.Value = DateTime.Parse(customerInfo.date_checkin.ToString());
            datepickDateCheckOut.Value = DateTime.Parse(customerInfo.date_checkout.ToString());
            txtRoomName.Text = customerInfo.roomtypename + " " + customerInfo.roomcatename;
            totalRoom =(int)customerInfo.roomcount;

            txtTotalRoom.Text = totalRoom.ToString() + " Room";
            totalprice = (decimal)customerInfo.Prices;
            txtPrice.Text = totalprice.ToString() + " $";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            frmPayment payment = new frmPayment(totalprice);
            if (payment.ShowDialog() == DialogResult.OK)
            {
                decimal customerMoney = payment.moneylist[0];
                decimal changeMoney = payment.moneylist[1];
                decimal totalRoomPrice = totalprice;
                customer.ConFirmBooking(bookingId, customerName, totalRoom, totalRoomPrice, totalprice, customerMoney, changeMoney);
            }
            frmMain main = new frmMain();
            MetroFramework.MetroMessageBox.Show(main, "Congratulation! Booking Scucessfully", "Add New Booking", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
