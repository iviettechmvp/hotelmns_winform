﻿using BUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTA;

namespace HotelMNS_WF
{
    public partial class frmCheckOut : MetroFramework.Forms.MetroForm
    {
        private RoomMapBUS roomMapBus = new RoomMapBUS();
        int bookingid;
        public frmCheckOut()
        {
            InitializeComponent();
        }

        private void frmCheckOut_Load(object sender, EventArgs e)
        {
            frmMain.tableDependency.Stop();
        }

        private void btnCacncelCheckIn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtRoom.Text != "")
            {
                int roomid = Int32.Parse(txtRoom.Text);
                List<spGetRoomOccupied_Result> listroom = roomMapBus.GetRoomForCheckOut();
                foreach(var item in listroom)
                {
                    if (item.roomid == roomid)
                        bookingid = item.bookingid;
                }
                spGetInfoOfRoomOccupied_Result roominfo = roomMapBus.GetInfoOfRoom(roomid, bookingid);
                txtCustomerName.Text = roominfo.first_name + " " + roominfo.last_name;
                txtBookingId.Text = bookingid.ToString();
                txtEmail.Text = roominfo.email.ToString();
                txtCity.Text = roominfo.citizenship.ToString();
                txtPhoneNumber.Text = roominfo.phone_number.ToString();
                txtIdCard.Text = roominfo.id_cardnumber.ToString();
                txtIdCardType.Text = roominfo.idcard_type.ToString();
                dpickDateArrival.Value = DateTime.Parse(roominfo.date_checkin.ToString());
                dpickDateDepart.Value = DateTime.Parse(roominfo.date_checkout.ToString());
                List<spGetlistroomCustomerOccupied_Result> listroomOccupied = roomMapBus.GetlistRoomCustomerOccupied(bookingid);
                string room = "";
                foreach(var i in listroomOccupied)
                {
                    room += i.Room.ToString() + " ";
                }
                //dgvListService.DataSource = null;
                //dgvListService.Rows.Clear();
                //dgvListService.Refresh();
                List<spGetlistServiceRoom_Result> GetServiceUse = roomMapBus.GetServiceUse(bookingid);
                dgvListService.DataSource = GetServiceUse;
            }
        }

        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            if(MetroFramework.MetroMessageBox.Show(this,"Are you Sure want to check out ?","Warning",MessageBoxButtons.OKCancel,MessageBoxIcon.Warning)== DialogResult.OK)
            {
                roomMapBus.UpdateCustomerBookingCheckOut(bookingid);
            }
            frmMain.tableDependency.Start();
        }
    }
}
