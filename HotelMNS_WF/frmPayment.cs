﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelMNS_WF
{
    public partial class frmPayment : MetroFramework.Forms.MetroForm
    {
        decimal totalprice;
        public decimal[] moneylist = new decimal[2];
        public frmPayment(decimal price)
        {
            this.totalprice = price;
            InitializeComponent();
        }

        private void metroLinkLabel1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmPayment_Load(object sender, EventArgs e)
        {
            txtTotalPrice.Text = totalprice.ToString();
        }

        private void txtCustomerMoney_TextChanged(object sender, EventArgs e)
        {
            decimal customerPrice = Decimal.Parse(txtCustomerMoney.Text);
            decimal change = customerPrice - totalprice;
            moneylist[0] = customerPrice;
            moneylist[1] = change;
            txtChange.Text = change.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMain main = new frmMain();
            DialogResult dr = MetroFramework.MetroMessageBox.Show(main, "Are You Confirm", "Confirm Payment", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if(dr == DialogResult.OK)
            {
                this.Close();
            }
        }
    }
}
