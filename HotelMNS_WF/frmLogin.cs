﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;

namespace HotelMNS_WF
{
    public partial class frmLogin : MetroFramework.Forms.MetroForm
    {
        private static string username;
        private static string floginname;

        public static string fLoginName
        {
            get { return floginname; }
            set { floginname = value; }
        }
        private static frmLogin _instance;

        UserBUS user = new UserBUS();
        bool hidden = true;

        public static frmLogin Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new frmLogin();
                return _instance;
            }

            set
            {
                _instance = value;
            }
        }

        public static string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
            }
        }

        public frmLogin()
        {
            InitializeComponent();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnPassword_MouseHover(object sender, EventArgs e)
        {
            btnPassword.BackColor = Color.FromArgb(0, 191, 165);
        }

        private void btnPassword_Click(object sender, EventArgs e)
        {
            Username = txtUserName.Text;
            string password = txtPassword.Text;
            if (user.LoginAuth(Username, password))
            {
                user.ChangeStatus(Username);
                fLoginName = user.GetNameUser(Username);
                this.Hide();
                frmMain.IstanceMain.Show();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "Login Faild", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnShowPassword_Click(object sender, EventArgs e)
        {
            if (hidden)
            {
                txtPassword.PasswordChar = '\0';
                hidden = false;
            }
            else
            {
                txtPassword.PasswordChar = '*';
                hidden = true;
            }
        }

        private void btnForgotPassword_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmForgotPassword.Instanceforgot.Show();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            _instance = this;
        }
    }
}
