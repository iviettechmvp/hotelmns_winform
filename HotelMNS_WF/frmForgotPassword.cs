﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using BUS;

namespace HotelMNS_WF
{
    public partial class frmForgotPassword : MetroFramework.Forms.MetroForm
    {
        UserBUS user = new UserBUS();
        private static frmForgotPassword _instanceforgot;

        public static frmForgotPassword Instanceforgot
        {
            get
            {
                if (_instanceforgot == null)
                    _instanceforgot = new frmForgotPassword();
                return _instanceforgot;
            }

            set
            {
                _instanceforgot = value;
            }
        }

        public frmForgotPassword()
        {
            InitializeComponent();
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin.Instance.Show();
        }

        private void frmForgotPassword_Load(object sender, EventArgs e)
        {
            _instanceforgot = this;
        }

        private void btnSendEmail_Click(object sender, EventArgs e)
        {
            string body = "Your Password "+user.GetForgotPassword(txtEmail.Text);
            MailMessage mail = new MailMessage("sinhvienbk17@gmail.com", txtEmail.Text,"Forgot Password",body);
            SmtpClient client = new SmtpClient("smtp.gmail.com");
            client.Port = 587;
            client.Credentials = new NetworkCredential("sinhvienbk17@gmail.com", "bachkhoadn17");
            client.EnableSsl = true;
            client.Send(mail);
            MetroFramework.MetroMessageBox.Show(this, "Email has been sent", "Information", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }
    }
}
