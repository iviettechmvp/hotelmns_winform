﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSuite;
using HotelMNS_WF.Shared;
using TableDependency.Enums;
using TableDependency.EventArgs;
using TableDependency.SqlClient;
using DTA;
using HotelMNS_WF.Shared.Accomodation;
using HotelMNS_WF.Shared.Dashboard;

namespace HotelMNS_WF
{
    public partial class frmMain : MetroFramework.Forms.MetroForm
    {
        UserDB user = new UserDB();
        private static frmMain _istanceMain;

        int count = 1;
        int idbooking;
        public int bookingstoreid
        {
            get { return idbooking; }
            set { idbooking = value; }
        }
        int bookingStoreStatus;
        static string connection = "Data Source=TOMMY;Initial Catalog=HotelMNS;Integrated Security=True";
        private static SqlTableDependency<Booking> tbdepency;
        public static SqlTableDependency<Booking> tableDependency
        {
            get
            {
                if (tbdepency == null)
                    tbdepency = new SqlTableDependency<Booking>(connection, "Booking");
                return tbdepency;
            }
            set
            {
                tbdepency = value;
            }
        }

        public MetroFramework.Controls.MetroPanel MetroContainer
        {
            get { return mPanelMainAccomon; }
            set { mPanelMainAccomon = value; }
        }

        public static frmMain IstanceMain
        {
            get
            {
                if (_istanceMain == null)
                    _istanceMain = new frmMain();
                return _istanceMain;
            }

            set
            {
                _istanceMain = value;
            }
        }

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            this.WindowState = FormWindowState.Maximized;
            this.ControlBox = false;
            this.FormBorderStyle = FormBorderStyle.None;
            lbUserName.Text = frmLogin.fLoginName;
            lbUserNameWel.Text = frmLogin.fLoginName;
            LoadDashboard();
            StartListen();
        }

        private void btnlogout_Click(object sender, EventArgs e)
        {
            if(MetroFramework.MetroMessageBox.Show(this,"Are you sure want to log out ?","warning",MessageBoxButtons.OKCancel,MessageBoxIcon.Stop) == DialogResult.OK)
            {
                this.Hide();
                user.Logout(frmLogin.Username);
                frmLogin.Instance.Show();
            }
        }
        public void LoadDashboard()
        {

            mPanelMainAccomon.Controls.Add(_MainDashboard.Instance);
            _MainDashboard.Instance.Dock = DockStyle.Fill;
            _MainDashboard.Instance.BringToFront();
        }

        private void btnAccomondation_Click(object sender, EventArgs e)
        {
            if (!mPanelMainAccomon.Controls.Contains(_MainAccomon.Instance))
            {
                mPanelMainAccomon.Controls.Add(_MainAccomon.Instance);
                _MainAccomon.Instance.Dock = DockStyle.Fill;
                _MainAccomon.Instance.BringToFront();
            }
            else
                _MainAccomon.Instance.BringToFront();
        }
        public void StartListen()
        {
            tableDependency.OnChanged += TableDependency_Changed;
            tableDependency.OnError += TableDependency_Error;
            tableDependency.Start();
        }
        public void TableDependency_Changed(object sender, RecordChangedEventArgs<Booking> e)
        {
            if (e.ChangeType != ChangeType.None)
            {
                var changedEntity = e.Entity;
                int bookingid = changedEntity.id;
                int bookingStatus = (int)changedEntity.Status;
                if (count == 1)
                {
                    frmShowNotification frmCusmbooking = new frmShowNotification(bookingid);
                    if (frmCusmbooking.ShowDialog() == DialogResult.OK)
                    {
                        bookingstoreid = bookingid;
                        bookingStoreStatus = bookingStatus;
                        count++;
                    }
                }
                else
                {
                    if (bookingid == bookingstoreid)
                        return;
                    else
                    {
                        frmShowNotification frmCusmbooking = new frmShowNotification(bookingid);
                        if (frmCusmbooking.ShowDialog() == DialogResult.OK)
                        {
                            bookingstoreid = bookingid;
                            bookingStoreStatus = bookingStatus;
                        }
                    }
                }
            }

        }
        public void TableDependency_Error(object sender, ErrorEventArgs e)
        {
            Exception ex = e.Error;
            throw ex;
        }

        private void btnDasboard_Click(object sender, EventArgs e)
        {
            _MainDashboard.Instance.BringToFront();
        }

        private void mPanelMainAccomon_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
