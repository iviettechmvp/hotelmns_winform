﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTA;
using MaterialSkin.Controls;

namespace HotelMNS_WF.Shared.Accomodation
{
    public partial class btnCheckOut : UserControl
    {
        private RoomMapBUS roomMapBus = new RoomMapBUS();
        private int index;
        private bool isSelect = true;
        public btnCheckOut()
        {
            InitializeComponent();
        }

        private void _Room_Load(object sender, EventArgs e)
        {
            Init();
            dgvListRoom.DataSource = roomMapBus.GetAllListRoomOccupied();
        }

        public void Init()
        {
            dgvListRoom.AutoGenerateColumns = false;
            dgvListRoom.Columns.Add("clsroomid", "Room Number");
            dgvListRoom.Columns[0].DataPropertyName = "roomid";

            dgvListRoom.Columns.Add("clsroomname", "Room Name");
            dgvListRoom.Columns[1].DataPropertyName = "roomname";

            dgvListRoom.Columns.Add("clstype", "Room Type");
            dgvListRoom.Columns[2].DataPropertyName = "roomtype_name";

            dgvListRoom.Columns.Add("clscategory", "Room Category");
            dgvListRoom.Columns[3].DataPropertyName = "roomcate_name";

            dgvListRoom.Columns.Add("clsmaxcapa", "Capacility");
            dgvListRoom.Columns[4].DataPropertyName = "capacility";

            dgvListRoom.Columns.Add("clsdescription", "Description");
            dgvListRoom.Columns[5].DataPropertyName = "description";

            dgvListRoom.Columns.Add("clsprice", "Prices");
            dgvListRoom.Columns[6].DataPropertyName = "price";

            dgvListRoom.Columns.Add("clsstatusname", "Status'Name");
            dgvListRoom.Columns[7].DataPropertyName = "roomstatus";
            dgvListRoom.Columns[7].Visible = false;

            dgvListRoom.Columns.Add("clsisavai", "Is vaiable");
            dgvListRoom.Columns[8].DataPropertyName = "isAvaiable";
            dgvListRoom.Columns[8].Visible = false;

            dgvListRoom.Columns.Add("clsroomtypeid", "Room type Id");
            dgvListRoom.Columns[9].DataPropertyName = "roomtype_id";
            dgvListRoom.Columns[9].Visible = false;

            dgvListRoom.Columns.Add("clscategory", "Room Category Id");
            dgvListRoom.Columns[10].DataPropertyName = "roomcate_id";
            dgvListRoom.Columns[10].Visible = false;

            dgvListRoom.Columns.Add("clsbookingid", "Booking Id");
            dgvListRoom.Columns[11].DataPropertyName = "bookingid";
            dgvListRoom.Columns[11].Visible = false;



            dgvListRoom.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvListRoom.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void dgvListRoomMap_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //index = dgvListRoom.CurrentCell.RowIndex;
        }

        private void LoadRowInfo()
        {
            if (isSelect)
            {
                if (index >= 0)
                {
                    int roomID = Int32.Parse(dgvListRoom.Rows[index].Cells[0].Value.ToString());
                    int bookingId = Int32.Parse(dgvListRoom.Rows[index].Cells[11].Value.ToString());
                    spGetInfoOfRoomOccupied_Result roomInfo = roomMapBus.GetInfoOfRoom(roomID, bookingId);
                    txtCustomerName.Text = roomInfo.first_name.Trim() + " " + roomInfo.last_name.Trim();
                    txtIdCard.Text = roomInfo.id_cardnumber.ToString();
                    txtBookingId.Text = bookingId.ToString();
                    txtIdCardType.Text = roomInfo.idcard_type.ToString().Trim();
                    datepickCheckIn.Value = DateTime.Parse(roomInfo.date_checkin.ToString());
                    txtPhoneNumber.Text = roomInfo.phone_number.ToString().Trim();
                    datepickCheckOut.Value = DateTime.Parse(roomInfo.date_checkout.ToString());
                    txtEmail.Text = roomInfo.email;
                    txtNotes.Text = (roomInfo.notes == null) ? "No Request" : roomInfo.notes;
                    lbRoomNumber.Text = roomID.ToString();
                    CheckStatusRoom();
                    txtCitizenship.Text = roomInfo.citizenship.ToString();
                    if (roomInfo.gender == true)
                        radMale.Checked = true;
                    else
                        radFemale.Checked = true;
                }
            }
        }

        public void CheckStatusRoom()
        {
            switch (dgvListRoom.Rows[index].Cells[7].Value.ToString())
            {
                case "Avaiable":
                    chkboxStAvaiable.Checked = true;
                    chkboxStAvaiable.Enabled = true;
                    chkboxStOccupied.Checked = chkboxStFixing.Checked = chkboxStCleaning.Checked = chkboxStCusWalking.Checked = chkboxStDirty.Checked = !chkboxStAvaiable.Checked;
                    chkboxStOccupied.Enabled = chkboxStFixing.Enabled = chkboxStCleaning.Enabled = chkboxStCusWalking.Enabled = chkboxStDirty.Enabled = !chkboxStAvaiable.Enabled;
                    break;
                case "Occupied":
                    chkboxStOccupied.Checked = true;
                    chkboxStOccupied.Enabled = true;
                    chkboxStAvaiable.Checked = chkboxStFixing.Checked = chkboxStCleaning.Checked = chkboxStCusWalking.Checked = chkboxStDirty.Checked = !chkboxStOccupied.Checked;
                    chkboxStAvaiable.Enabled = chkboxStFixing.Enabled = chkboxStCleaning.Enabled = chkboxStCusWalking.Enabled = chkboxStDirty.Enabled = !chkboxStOccupied.Enabled;
                    break;
                case "Fixing":
                    chkboxStFixing.Checked = true;
                    chkboxStFixing.Enabled = true;
                    chkboxStAvaiable.Checked = chkboxStOccupied.Checked = chkboxStCleaning.Checked = chkboxStCusWalking.Checked = chkboxStDirty.Checked = !chkboxStFixing.Checked;
                    chkboxStAvaiable.Enabled = chkboxStOccupied.Enabled = chkboxStCleaning.Enabled = chkboxStCusWalking.Enabled = chkboxStDirty.Enabled = !chkboxStFixing.Enabled;
                    break;
                case "Customer Walking":
                    chkboxStCusWalking.Checked = true;
                    chkboxStCusWalking.Enabled = true;
                    chkboxStAvaiable.Checked = chkboxStOccupied.Checked = chkboxStCleaning.Checked = chkboxStFixing.Checked = chkboxStDirty.Checked = !chkboxStCusWalking.Checked;
                    chkboxStAvaiable.Enabled = chkboxStOccupied.Enabled = chkboxStCleaning.Enabled = chkboxStFixing.Enabled = chkboxStDirty.Enabled = !chkboxStCusWalking.Enabled;
                    break;
                case "Cleaning":
                    chkboxStCleaning.Checked = true;
                    chkboxStCleaning.Enabled = true;
                    chkboxStAvaiable.Checked = chkboxStOccupied.Checked = chkboxStCusWalking.Checked = chkboxStFixing.Checked = chkboxStDirty.Checked = !chkboxStCleaning.Checked;
                    chkboxStAvaiable.Enabled = chkboxStOccupied.Enabled = chkboxStCusWalking.Enabled = chkboxStFixing.Enabled = chkboxStDirty.Enabled = !chkboxStCleaning.Enabled;
                    break;
                case "Dirty":
                    chkboxStDirty.Checked = true;
                    chkboxStDirty.Enabled = true;
                    chkboxStAvaiable.Checked = chkboxStOccupied.Checked = chkboxStCusWalking.Checked = chkboxStFixing.Checked = chkboxStCleaning.Checked = !chkboxStDirty.Checked;
                    chkboxStAvaiable.Enabled = chkboxStOccupied.Enabled = chkboxStCusWalking.Enabled = chkboxStFixing.Enabled = chkboxStCleaning.Enabled = !chkboxStDirty.Enabled;
                    break;
            }
        }
        private void dgvListRoomMap_SelectionChanged(object sender, EventArgs e)
        {
            index = dgvListRoom.CurrentCell.RowIndex;
            LoadRowInfo();
        }

        private void btnCheckIn_Click(object sender, EventArgs e)
        {
            frmCheckIn checkinfrm = new frmCheckIn();
            if(checkinfrm.ShowDialog() == DialogResult.OK)
            {
                MetroFramework.MetroMessageBox.Show(this, "Check In Successfully", "Check In", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dgvListRoom.DataSource = null;
                dgvListRoom.DataSource = roomMapBus.GetAllListRoomOccupied();
            }

        }

        private void dgvListRoom_DoubleClick(object sender, EventArgs e)
        {
            int roomid = Int32.Parse(dgvListRoom.Rows[index].Cells[0].Value.ToString());
            int bookingId = Int32.Parse(dgvListRoom.Rows[index].Cells[11].Value.ToString());
            frmAddService fservice = new frmAddService(roomid,bookingId);
            fservice.ShowDialog();
        }

        private void txtSearchRoom_TextChanged(object sender, EventArgs e)
        {
           
        }
        private void btnCheckOutCustomer_Click(object sender, EventArgs e)
        {
            frmCheckOut fCheckout = new frmCheckOut();
            if (fCheckout.ShowDialog() == DialogResult.OK)
            {
                MetroFramework.MetroMessageBox.Show(this, "Check out Succesfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dgvListRoom.DataSource = null;
                dgvListRoom.DataSource = roomMapBus.GetAllListRoomOccupied();
                Clear();
            }
        }
        public void Clear()
        {
            txtCustomerName.Text = "";
            txtBookingId.Text = "";
            txtCitizenship.Text = "";
            txtIdCard.Text = "";
            txtIdCardType.Text = "";
            txtNotes.Text = "";
            txtPhoneNumber.Text = "";
            chkboxStAvaiable.Checked = false;
            chkboxStCleaning.Checked = false;
            chkboxStCusWalking.Checked = false;
            chkboxStDirty.Checked = false;
            chkboxStFixing.Checked = false;
            chkboxStOccupied.Checked = false;
            radMale.Checked = false;
            radFemale.Checked = false;
        }
    }
}
