﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelMNS_WF.Shared.Accomodation
{
    public partial class _MainAccomon : UserControl
    {
        private static _MainAccomon _instance;

        public static _MainAccomon Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new _MainAccomon();
                return _instance;
            }

        }
        public _MainAccomon()
        {
            InitializeComponent();

        }

        private void _MainAccomon_Load(object sender, EventArgs e)
        {
            btnCheckOut ucRoom = new btnCheckOut();
            ucRoom.Dock = DockStyle.Fill;
            LoadUc(ucRoom);
        }
        public void LoadUc(UserControl uc)
        {
            panelLoadUcAccomon.Controls.Add(uc);
        }
    }
}
