﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTA;
using BUS;
using Newtonsoft.Json;
using System.Net;

namespace HotelMNS_WF.Shared.Dashboard
{
    public partial class _MainDashboard : UserControl
    {
        Timer t = new Timer();
        UserBUS user = new UserBUS();
        private static _MainDashboard _instance;

        public static _MainDashboard Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new _MainDashboard();
                return _instance;
            }

        }
        public _MainDashboard()
        {
            InitializeComponent();
        }

        private void _MainDashboard_Load(object sender, EventArgs e)
        {
            t.Interval = 1000;
            t.Tick += new EventHandler(this.t_Tick);
            t.Start();
            LoadContent();
        }
        private void t_Tick(object sender, EventArgs e)
        {
            int hh = DateTime.Now.Hour;
            int mm = DateTime.Now.Minute;
            int ss = DateTime.Now.Second;
            string time = "";
            if (hh < 10)
            {
                time += "0" + hh;
            }
            else
            {
                time += hh;
            }
            time += ":";
            if (mm < 10)
            {
                time += "0" + mm;
            }
            else
            {
                time += mm;
            }
            time += ":";
            if (ss < 10)
            {
                time += "0" + ss;
            }
            else
            {
                time += ss;
            }
            lbTimeNow.Text = time;
        }
        public void LoadContent()
        {
            var client = new WebClient();
            lbUserCount.Text = user.UserCount().ToString();
            lbCustomerCount.Text = user.CustomerCount().ToString();
            lbBookingCount.Text = user.BookingCount().ToString();
            lbRoomCount.Text = user.RoomCount().ToString();
            lbServiceCount.Text = user.ServiceCount().ToString();
            lbFacilityCount.Text = user.FacilitiesCount().ToString();

            //Load Weather
            var responseStr = client.DownloadString("http://api.openweathermap.org/data/2.5/weather?id=1905468&appid=f79a2f3aad5387735467defccbd73a6a&units=metric");
            dynamic m = JsonConvert.DeserializeObject<dynamic>(responseStr);
            var tempuarate = m.main.temp;
            var description = m.weather[0].description;
            lbtempuarate.Text = tempuarate;
            lbDescriptionweather.Text = description;

            // LoadDate
            lbDateNow.Text = DateTime.Now.ToShortDateString();
        }
    }
}
