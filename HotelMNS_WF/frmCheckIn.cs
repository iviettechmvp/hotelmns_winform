﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTA;
using TableDependency.Enums;
using TableDependency.EventArgs;
using TableDependency.SqlClient;

namespace HotelMNS_WF
{
    public partial class frmCheckIn : MetroFramework.Forms.MetroForm
    {
        CustomerInfoBUS customer = new CustomerInfoBUS();
        int bookingid;
        int cusomerid;
        List<int> collectroomid;
        frmMain main = new frmMain();
        public frmCheckIn()
        {
            InitializeComponent();
        }

        private void frmCheckIn_Load(object sender, EventArgs e)
        {
            frmMain.tableDependency.Stop();
            LoadInfoCustomer();
        }

        public void LoadInfoCustomer()
        {
            cboCustomerName.DataSource = customer.GetAllCustomerBookingConfirmed();
            cboCustomerName.DisplayMember = "customername";
        }

        private void btnCacncelCheckIn_Click(object sender, EventArgs e)
        {

            if(MetroFramework.MetroMessageBox.Show(main,"Do You Want Cancel CheckIn ? ","Cancel Booking",MessageBoxButtons.OKCancel,MessageBoxIcon.Warning) == DialogResult.OK)
            {
                frmMain.tableDependency.Start();
                this.Close();
            }
        }

        private void btnCheckIn_Click(object sender, EventArgs e)
        {

            if(MetroFramework.MetroMessageBox.Show(this,"Do You Want Check In This Customer? ","Confirm",MessageBoxButtons.OKCancel,MessageBoxIcon.Warning)== DialogResult.OK)
            {
                customer.UpdateCustomerBookingCheckIn(cusomerid, bookingid, txtIdCard.Text, txtIdCardType.Text, collectroomid);
            }
            frmMain.tableDependency.Start();
        }
        private void cboCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            spGetAllCustomerBookingConfirm_Result customerbooking = (spGetAllCustomerBookingConfirm_Result)cboCustomerName.SelectedItem;
            bookingid = customerbooking.bookingid; // booking id
            cusomerid = customerbooking.id;
            txtEmail.Text = customerbooking.email;
            txtPhoneNumber.Text = customerbooking.phone_number;
            txtCity.Text = customerbooking.citizenship;
            dpickDateArrival.Value = (DateTime)customerbooking.date_checkin;
            dpickDateDepart.Value = (DateTime)customerbooking.date_checkout;
            List<spGetRoomBooked_Result> listroom = customer.GetRoomBooking(bookingid);
            string showRoom = "";

            // Collection roomid
            List<int> listroomid = new List<int>();
            foreach (var item in listroom)
            {
                listroomid.Add(item.Room);
                showRoom += item.Room.ToString() + " ";
            }

            // assign new list
            collectroomid = listroomid;
            txtRoom.Text = showRoom;
            txtBookingId.Text = bookingid.ToString();

            if (customerbooking.gender == true)
            {
                radMale.Checked = true;
            }
            else
                radFemale.Checked = true;
            txtNote.Text = customerbooking.notes;

            // Store bookingId
            main.bookingstoreid = bookingid;
        }
    }
}
