﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTA
{
    public class UserDB
    {
        HotelMNSEntities db = new HotelMNSEntities();

        public bool Login(string username, string password)
        {
            User cuser = (from u in db.Users
                          where u.username.Equals(username)
                            && u.password.Equals(password)
                          select u).FirstOrDefault();
            if (cuser == null)
            {
                return false;
            }

            else
            {
                return true;
            }
        }

        public string GetPassword(string email)
        {
            User user = (from u in db.Users
                         where u.email.Equals(email)
                         select u).FirstOrDefault();
            if (user == null) return null;
            else
            {
                string password = user.password;
                return password;
            }
        }
        public string GetName(string username)
        {
            User cuser = (from u in db.Users
                          where u.username.Equals(username)
                          select u).FirstOrDefault();
            string name = cuser.name;
            return name;
        }
        public void UpdateStatus(string username)
        {
            User cuser = (from u in db.Users
                              where u.username.Equals(username)
                              select u).FirstOrDefault();
            cuser.status = true;
            db.SaveChanges();
        }
        public void Logout(string username)
        {
            User cuser = (from u in db.Users
                          where u.username.Equals(username)
                          select u).FirstOrDefault();
            cuser.status = false;
            db.SaveChanges();
        }

        public int CountUser()
        {
            var count = db.Users.Count();
            return count;
        }
        public int CountCustomer()
        {
            var count = db.Customers.Count();
            return count;
        }
        public int CountBooking()
        {
            var count = db.Bookings.Count();
            return count;
        }
        public int CountRoom()
        {
            var count = db.Rooms.Count();
            return count;
        }
        public int CountService()
        {
            var count = db.Services.Count();
            return count;
        }
        public int CountFacilities()
        {
            var count = db.Facilities.Count();
            return count;
        }
    }
}
