//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DTA
{
    using System;
    
    public partial class spGetInvoice_Result
    {
        public int id { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<int> Booking { get; set; }
        public string customer_name { get; set; }
        public Nullable<int> total_service { get; set; }
        public Nullable<decimal> total_priceservice { get; set; }
        public Nullable<int> total_room { get; set; }
        public Nullable<decimal> total_roomprice { get; set; }
        public Nullable<decimal> totalprice { get; set; }
        public Nullable<decimal> total_customerpaid { get; set; }
        public Nullable<decimal> chaneg_money { get; set; }
    }
}
