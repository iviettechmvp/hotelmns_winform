namespace DTA.Entity
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class HotelModelDataContext : DbContext
    {
        public HotelModelDataContext()
            : base("name=HotelModelDataContext")
        {
        }

        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<Booking_Room> Booking_Room { get; set; }
        public virtual DbSet<Booking_Service> Booking_Service { get; set; }
        public virtual DbSet<Cancel_Booking> Cancel_Booking { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Occupied_Room> Occupied_Room { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<Room_Category> Room_Category { get; set; }
        public virtual DbSet<Room_Price> Room_Price { get; set; }
        public virtual DbSet<Room_Status> Room_Status { get; set; }
        public virtual DbSet<Room_Type> Room_Type { get; set; }
        public virtual DbSet<Service> Services { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Booking>()
                .Property(e => e.deposits)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Booking>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<Booking>()
                .HasMany(e => e.Booking_Room)
                .WithRequired(e => e.Booking1)
                .HasForeignKey(e => e.Booking);

            modelBuilder.Entity<Booking>()
                .HasMany(e => e.Booking_Service)
                .WithRequired(e => e.Booking1)
                .HasForeignKey(e => e.Booking);

            modelBuilder.Entity<Booking>()
                .HasMany(e => e.Cancel_Booking)
                .WithRequired(e => e.Booking1)
                .HasForeignKey(e => e.Booking)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Booking>()
                .HasMany(e => e.Occupied_Room)
                .WithRequired(e => e.Booking1)
                .HasForeignKey(e => e.Booking)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cancel_Booking>()
                .Property(e => e.reason)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.first_name)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.last_name)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.phone_number)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.citizenship)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.idcard_type)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Bookings)
                .WithRequired(e => e.Customer1)
                .HasForeignKey(e => e.Customer);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Cancel_Booking)
                .WithRequired(e => e.Customer1)
                .HasForeignKey(e => e.Customer);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Occupied_Room)
                .WithRequired(e => e.Customer1)
                .HasForeignKey(e => e.Customer);

            modelBuilder.Entity<Room>()
                .HasMany(e => e.Booking_Room)
                .WithRequired(e => e.Room1)
                .HasForeignKey(e => e.Room);

            modelBuilder.Entity<Room>()
                .HasMany(e => e.Occupied_Room)
                .WithRequired(e => e.Room1)
                .HasForeignKey(e => e.Room);

            modelBuilder.Entity<Room_Category>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<Room_Category>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<Room_Category>()
                .HasMany(e => e.Rooms)
                .WithOptional(e => e.Room_Category1)
                .HasForeignKey(e => e.Room_Category)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Room_Category>()
                .HasMany(e => e.Room_Price)
                .WithRequired(e => e.Room_Category1)
                .HasForeignKey(e => e.Room_Category);

            modelBuilder.Entity<Room_Price>()
                .Property(e => e.price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Room_Status>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<Room_Status>()
                .HasMany(e => e.Rooms)
                .WithOptional(e => e.Room_Status1)
                .HasForeignKey(e => e.Room_Status)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Room_Type>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<Room_Type>()
                .HasMany(e => e.Rooms)
                .WithOptional(e => e.Room_Type1)
                .HasForeignKey(e => e.Room_Type)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Room_Type>()
                .HasMany(e => e.Room_Price)
                .WithRequired(e => e.Room_Type1)
                .HasForeignKey(e => e.Room_Type);

            modelBuilder.Entity<Service>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.price)
                .IsFixedLength();

            modelBuilder.Entity<Service>()
                .HasMany(e => e.Booking_Service)
                .WithRequired(e => e.Service1)
                .HasForeignKey(e => e.Service);
        }
    }
}
