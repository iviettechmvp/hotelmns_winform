namespace DTA.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cancel_Booking
    {
        [Key]
        [Column(Order = 0)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Customer { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Booking { get; set; }

        public DateTime? date_cancel { get; set; }

        [Column(TypeName = "text")]
        public string reason { get; set; }

        public virtual Booking Booking1 { get; set; }

        public virtual Customer Customer1 { get; set; }
    }
}
