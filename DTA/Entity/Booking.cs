namespace DTA.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Booking")]
    public partial class Booking
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Booking()
        {
            Booking_Room = new HashSet<Booking_Room>();
            Booking_Service = new HashSet<Booking_Service>();
            Cancel_Booking = new HashSet<Cancel_Booking>();
            Occupied_Room = new HashSet<Occupied_Room>();
        }

        public int id { get; set; }

        public DateTime? created_date { get; set; }

        public DateTime? date_checkin { get; set; }

        public DateTime? date_checkout { get; set; }

        public int? children_number { get; set; }

        public int? adult_number { get; set; }

        public decimal? deposits { get; set; }

        [Column(TypeName = "text")]
        public string notes { get; set; }

        public int Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Booking_Room> Booking_Room { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Booking_Service> Booking_Service { get; set; }

        public virtual Customer Customer1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cancel_Booking> Cancel_Booking { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Occupied_Room> Occupied_Room { get; set; }
    }
}
