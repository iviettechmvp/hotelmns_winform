namespace DTA.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Room")]
    public partial class Room
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Room()
        {
            Booking_Room = new HashSet<Booking_Room>();
            Occupied_Room = new HashSet<Occupied_Room>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public int? adult_number { get; set; }

        public int? children_number { get; set; }

        public int? Room_Type { get; set; }

        public int? Room_Category { get; set; }

        public int? Room_Status { get; set; }

        public bool? isAvaiable { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Booking_Room> Booking_Room { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Occupied_Room> Occupied_Room { get; set; }

        public virtual Room_Category Room_Category1 { get; set; }

        public virtual Room_Status Room_Status1 { get; set; }

        public virtual Room_Type Room_Type1 { get; set; }
    }
}
