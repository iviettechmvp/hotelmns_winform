﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTA
{
    public class CustomerInfoDB
    {
        HotelMNSEntities db = new HotelMNSEntities();

        public spGetCustomerJustBooking_Result GetInfo(int? bookingId)
        {
            return db.spGetCustomerJustBooking(bookingId).Single();
        }

        public void ConfirmBookingInvoice(int? bookingid, string customerName, int totalroom, decimal totalRoomPrice, decimal totalPrice, decimal totalCustomerpaid, decimal changeMoney, int toalService = 0, decimal totalPriceService = 0)
        {
            db.spUpdateCustomerBookingStatus(bookingid);
            Invoice invoice = new Invoice();
            invoice.Booking = bookingid;
            invoice.customer_name = customerName;
            invoice.total_service = toalService;
            invoice.total_priceservice = totalPriceService;
            invoice.total_room = totalroom;
            invoice.total_roomprice = totalRoomPrice;
            invoice.totalprice = totalPrice;
            invoice.total_customerpaid = totalCustomerpaid;
            invoice.chaneg_money = changeMoney;
            db.Invoices.Add(invoice);
            db.SaveChanges();
        }
        public List<spGetAllCustomerBookingConfirm_Result> GetCustomerConfirm()
        {
            return db.spGetAllCustomerBookingConfirm().ToList();
        }

        public List<spGetRoomBooked_Result> GetCustomerRoom(int? bookingid)
        {
            return db.spGetRoomBooked(bookingid).ToList();
        }
        public void UpdateBookingCheckIn(int customerId, int? bookingid, string idcard, string idcardType, List<int> roomid)
        {
            db.spUpdateBookingCheckIn(bookingid, idcard, idcardType);
            foreach(var item in roomid)
            {
                Occupied_Room ocupiedroom = new Occupied_Room();
                ocupiedroom.Customer = customerId;
                ocupiedroom.Booking = (int)bookingid;
                ocupiedroom.Room = item;
                ocupiedroom.RoomStatus = 2;
                db.Occupied_Room.Add(ocupiedroom);
                db.SaveChanges();
            }
        }
    }
}
